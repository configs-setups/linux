# Current full install
This guide/step-by-step log is supposed to be used after you have an installed instance of arch. Essentially, this is what comes after the [Arch Installation Guide](https://wiki.archlinux.org/title/installation_guide), and some VPSs may provide you the option to choose an Arch ISO and be at this stage from the get-go.


Any command not prefixed with `root` or `host` is to be ran on the target normal user account.

- **host:** `> ssh root@server-ip`
- **root:** `> sudo pacman -Syu`
- **root:** `> sudo pacman -S git curl wget neofetch dos2unix nmap openssh nginx mariadb php php-fpm php7 php7-fpm composer make cmake gcc htop`
- **root:** `> sudo pacman -S base-devel`



## Sudo
- **root:** `> useradd -m normal-user`
- **root:** `> usermod -aG wheel normal-user`
- add the following to `/root/.bashrc`, or suffer with Vi
	- `export EDITOR=/usr/bin/vim`
	- `export VISUAL=/usr/bin/vim`
- **root:** `> visudo`
	- uncomment [%wheel ALL=(ALL) all](../md-assets/sudoers-wheel.png)
- `> sudo whoami` and ensure you are `root`



## SSH to your server
- **host:** `> ssh-keygen -t ed25519 -C "your comment here"`
- **host:** edit `~/.ssh/config` according to [ssh-config-template](./ssh-config-template)
- **host:** `> ssh-copy-id -i ~/.ssh/your_ed25519_key.pub normal-user@server-ip"`. **Do** set a password
- Keep two terminals open, one as `root` and one as `normal-user`
- `sudo vim /etc/ssh/sshd_config` and set:
	- `PermitRootLogin no`
	- `PasswordAuthentication no`
	- `PubkeyAuthentication yes`
- `> sudo systemctl restart sshd`
- `> exit`
- **host:** `> ssh root@server-ip`, this should fail
- **host:** `> ssh normal-user@server-ip`, this should fail
- **host:** `> ssh cool-server-name` (where `cool-server-name` is the `Host` in `~/.ssh/config`). This should succeed



## SSH to platforms
- set up SSH keys for GitLab, GitHub, and whatever else:
	- `> ssh-keygen -t ed25519 -C "your comment here"`
	- you can use a unique key for every platform, and maybe you should.
	- edit your `~/.ssh/config` by adding the platforms and their respective details, see the [ssh-config-template](./ssh-config-template). Omit the `User` property for things like GitLab/GitHub
	- `> cat ~/.ssh/your_ed25519_key.pub` and copy (or copy with a CLI tool) and add the key to your account on the platform



## Hostname
- set your hostname in `/etc/hostname` and `/etc/hosts` (if applicable); no one likes defaultly generated names by server providers.
- `> sudo reboot`. This'll also potentially make sure you didn't royally fuck something, by potentially not allowing you back in if you did.



## MySQL
With terminal access, you can either use `> sudo mysql` to connect, or create another user. The user creation will be under phpMyAdmin section.

- `> sudo pacman -Syu`
- `> sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql` - see [Arch Wiki](https://wiki.archlinux.org/title/MariaDB#Installation)
- `> sudo systemctl enable mysql`
- `> sudo systemctl start mysql`
- `> sudo mysql_secure_installation`
	- `Switch to unix_socket authentication`: Y
	- `Change the root password?`: Y
	- `Remove anonymous users?`: Y
	- `Disallow root login remotely?`: Y
	- `Remove test database and access to it?`: Y
	- `Reload privilege tables now?`: Y



## NGINX
NGINX, at least on Arch, uses the `http` user, and either that group or user must be able to access the files you want NGINX to serve/use.



### http:http
Web-based folders/files will be owned by the `http` user/group, and you should add yourself to this group.
- `> sudo usermod -aG normal-user http`
- `> chown http:http ./your-folder-here`

This is the assumed way that things will be done.



### normal-user:http
Alternatively, your user alongisde the `http` group can be the owners:
- `> chown normal-user:http ./your-folder-here`



### NGINX continued
**Note** that you'll have to log out and back in for the group change to take effect... not knowing this has made me waste *days.*

- clone your nginx config repo, or whatever. You can also just re-write your conf files...
- grab/generate SSL certs
- `> mkdir /var/www/ /var/www/html` if they don't exist
- `> sudo chown -R http:http /var/www/`



## phpMyAdmin
Preferably host phpMyAdmin with an IP whitelist, on a hard-to-guess subdomain name.


- `> sudo pacman -S phpmyadmin`
- configure an NGINX site to root/alias `/usr/share/webapps/phpMyAdmin` with the correct php-fpm version (currently PHP 7).
	- set `client_max_body_size` to something higher to allow importing bigger SQL files.
- Access the page to see which PHP extensions are missing, then enable said PHP extension in `/etc/php7/php.ini` (or whichever PHP version)
- `> sudo mysql`
	- `CREATE USER 'phpmyadmin-user'@'localhost' IDENTIFIED BY 'password';` create a user for phpMyAdmin with a username and password that won't be easy to guess or bruteforce.
	- `GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin-user'@'localhost';` give the user all privileges
	- `GRANT GRANT OPTION ON *.* TO 'phpmyadmin-user'@'localhost';` if you want to grant permissions to other users
	- `FLUSH PRIVILEGES;`
- in `/etc/php7/php.ini`:
	- increase `upload_max_filesize`
	- increase `post_max_size` to something just below `upload_max_filesize`
- `> sudo chown -R http:http /usr/share/webapps/phpMyAdmin` not sure if this is necessary
- Generate a 32-char length "blowfish secret" and edit `$cfg['blowfish_secret']` in `/usr/share/webapps/phpMyAdmin/config.inc.php`
- Depending on the config, you will either need to:
	- create a `tmp` directory within phpMyAdmin;
	- or set `$cfg['TempDir'] = '/tmp';` in `config.inc.php`



## ufw
`> sudo pacman -S ufw`
`> sudo systemctl enable iptables`
`> sudo systemctl start iptables`
`> sudo ufw status`
`> sudo ufw allow ssh`
`> sudo ufw allow http`
`> sudo ufw allow https`
`> sudo ufw enable`

Allow at least ssh before enabling ufw



## fail2ban



## kanboard



## NodeJS env
- nvm




## asdf
- `> sudo pacman -S speedtest-cli`
- python?



## Enable/Start services
- `> sudo systemctl enable mysql`
- `> sudo systemctl enable nginx`
- `> sudo systemctl enable php-fpm`
- `> sudo systemctl enable php-fpm7`
<br>

- `> sudo systemctl start mysql`
- `> sudo systemctl start nginx`
- `> sudo systemctl start php-fpm`
- `> sudo systemctl start php-fpm7`
	- - actual config stuff


