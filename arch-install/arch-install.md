
# Install


## notes
- `git clone` AUR packages to `~/aur-packages`
- `git clone` other packages to `~/git-packages`, or combine the two into `~/git-packages`



## pacman-arch-chroot
Some basics to get started during arch-chroot.

`> pacman -S vim sudo efibootmgr dosfstools os-prober mtools iw iwd networkmanager dhcpcd base-devel git curl wget make cmake gcc ufw neofetch htop nano`


Additionally, if on WiFi:
`> pacman -S iwd iw`



## pacman-current-software
Mostly all the software I'll need, excluding anything that depends on the desktop env - e.g., `calcurse` is my current calendar choice on `i3`, but on KDE I use the default.

`> pacman -S code clang clang-tidy clang-format keychain speedtest-cli ttf-fira-code flameshot go golangci-lint firefox stress strace ntfs-3g discord thunderbird dos2unix ncdu shellcheck qalculate-gtk composer nmap nmon dnsutils man-pages jq zip unzip qalculate-gtk libcdio nomacs vlc ark dbeaver redis cloc neovim ranger highlight w3m`



## install-core
install/setup (WIP):
- `> loadkeys uk`
- `> ping google.com`
	- `> pacman -S networkmanager`
	- If on WiFi:
	- `> iwctl`
	- `> [iwd] device list`
	- `> [iwd] station <device> scan`
	- `> [iwd] station <device> get-networks`
	- `> [iwd] station <device> connect <network>`
- `> timedatectl set-ntp true`
- `> timedatectl status`
- `> fdisk -l` -> `fdisk` (create partitions as desired)
	- Create GPT partition table
	- 550M EFI partition
	- Swap partition 
	- Storage partition (Linux filesystem)
- `> mkfs.fat -F32 /dev/sda1`
- `> mkswap /dev/sda2`
- `> swapon /dev/sda2`
- `> mkfs.ext4 /dev/sda3`
- `> mount /dev/sda3 /mnt`
- `> pacstrap /mnt base linux linux-firmware`
- `> genfstab -U /mnt >> /mnt/etc/fstab`
- `> arch-chrott /mnt`


Everything now on /dev/sda3 root
- `> ln- -sf /usr/share/zoneinfo/Europe/London /etc/localtime`
- `> hwclock --systohc`
- `> pacman -S vim sudo`
- `vim /etc/locale.gen` -> uncomment en_GB UTF8
- `> locale-gen`
- `> vim /etc/hostname`
- `> vim /etc/hosts`

```bash
# /etc/hosts

127.0.0.1	localhost
::1			localhost
127.0.1.1	<hostname>.localdomain	<hostname>
```

- `> passwd`
- `> useradd -m <username>`
- `> passwd <username>`
- `> usermod -aG wheel,audio,video,optical <username>`
- `> pacman -S sudo`
- `> EDITOR=vim visudo`
	- uncomment [%wheel ALL=(ALL) all](../md-assets/sudoers-wheel.png)
- `> pacman -S efibootmgr dosfstools os-prober mtools`
- `> mkdir /boot/EFI`
- `> mount /dev/sda1 /boot/EFI`
- `> grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck`


Install required network stuff to not have to arch-chroot again
- `> pacman -S networkmanager dhcpcd`
	- If on WiFi
	- `> pacman -S iwd iw`
- `> systemctl enable NetworkManager`
- `> exit`

Reboot
- `> umount /mnt`
	- If fails, try
	- `> umount -f /mnt`
	- `> umount -l /mnt`


Note that after installing the core and being set up, you may want to disable iw/iwd and only keep NetworkManager enabled.

I've read that you can use iw as the backend for NetworkManager, and it may be faster or something, but that's for another day to look into.



## install-configure-ntfs
- `> sudo pacman -S ntfs-3g`



To have sane permissions:
- Get your user's `uid` and `gid` with `cat /etc/passwd | grep <username>`
- `> sudo vim /etc/fstab`
	- Edit the options of the selected drive to include `uid=1000,gid=1000,fmask=0022,dmask=0022`
	- Replace 1000 with your uid/gid
	- `0022` represents `chmod 755`
- `> umount /mnt-point` your drive
- `> sudo systemctl daemon-reload` to use the new fstab
- `> mount -a /dev/drive /mnt-point`
	- Without the `-a` it doesn't mount using the new options for some reason.



## install-ly-display-manager
- download release or clone https://github.com/fairyglade/ly
- `> make`
- `> sudo make run` to test run
- `> sudo make `
- `> sudo systemctl disable sddm` disable existing display manager, such as sddm
- `> sudo systemctl enable ly.service`
- `> sudo systemctl disable getty@tty2.service` to allow switching ttys and not show the ly login prompt on them


Some:
- uncomment the `x_cmd_setup` in `/etc/ly/config.ini`
- I needed to comment out my `exec i3` command in `~/.xinitrc` because opening links from Discord was broken, I assume ly does that itself and it conflicts? idk



## install-de-wayland
TBD

https://archlinux.org/groups/



## install-kde-x11
- `> sudo pacman -S xorg plasma`


WIP list, cherry-picking from kde-applications:
- `dolphin`
- `konsole`



## install-i3wm-x11
- `> sudo pacman -S xorg i3 dunst rofi hsetroot redshift picom brightnessctl rofi-greenclip pasystray pavucontrol calcurse ranger highlight w3m`



AUR packages:
- https://aur.archlinux.org/packages/betterlockscreen
- https://aur.archlinux.org/packages/i3lock-color


Configs and references:
- https://github.com/addy-dclxvi/i3-starterpack
- https://github.com/adi1090x/rofi
- launch `pasystray` for audio tray icon


TUI file managing is done with `ranger`, with `highlight` and `w3m` for code syntax and HTML/image preview.


GTK/KDE themes
- Some programs like Thunar are themed based on the system thingy, and I have been using KDE before I installed i3.
- - On a fresh install, I just installed plasma and selected `breeze`, `breeze-gtk`, and `systemsettings`



<br><br>



# Package Groups



## basic-packages
install/setup:
- `sudo pacman -S git curl wget neofetch zip unzip vim nano openssh`



<br>



## sysadmin-packages
install/setup:
- `sudo pacman -S htop stress speedtest-cli ntfs-3g ufw`



### ryzen-hardware
install/setup:
- install https://aur.archlinux.org/packages/zenpower3-dkms
- `lsmod | grep k10temp`
- `sudo modprobe -r k10temp` if the above showed it was active
- `sudo modprobe zenpower` to load new driver



<br>



## basic-media-packages
install/setup:
- `sudo pacman -S nomacs vlc mpv`



### extra-media-packages
install/setup:
- `sudo pacman -S libcdio`



### phone-media-packages
This is for accessing your phone's files. You may or may not need everything from this and it'll also depend on your file manager, e.g. [Thunar](https://wiki.archlinux.org/title/
thunar#Thunar_Volume_Manager)

install/setup:
- `sudo pacman -S mtpfs gfvs-mtp`

AUR packages:
- https://aur.archlinux.org/packages/jmtpfs




<br>



## basic-dev-packages
install/setup:
- `sudo pacman -S dos2unix ncdu shellcheck ttf-fira-code neovim`



### VSCode/Code/Codium
install/setup:
- Pick your preferred package: https://wiki.archlinux.org/title/Visual_Studio_Code#Installation
- `sudo pacman -S code`


Add Microsoft marketplace to `product.json`
- `/usr/lib/code/product.json`

```json
{
	"extensionsGallery": {
		"serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
		"cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
		"itemUrl": "https://marketplace.visualstudio.com/items",
		"controlUrl": "",
		"recommendationsUrl": ""
	}
}
```


documentation/reference:
- https://wiki.archlinux.org/title/Visual_Studio_Code



### SSH auto-completion
Arch for some reason doesn't have autocmplete of SSH hosts from within `~/.ssh/config`, but Ubuntu does.


Add the following to `~/.bashrc`:

```bash
_ssh() 
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=$(grep '^Host' ~/.ssh/config ~/.ssh/config.d/* 2>/dev/null | grep -v '[?*]' | cut -d ' ' -f 2-)

    COMPREPLY=( $(compgen -W "$opts" -- ${cur}) )
    return 0
}
complete -F _ssh ssh
```



Reference:
- https://unix.stackexchange.com/a/181603



### extra-dev-packages
install/setup:
- `sudo pacman -S nmap nmon dnsutils man-pages jq strace keychain cloc redis`



### cxx-dev
install/setup:
- `sudo pacman -S make cmake gcc clang clang-tidy clang-format`



### golang-dev
install/setup:
- `sudo pacman -S go golangci-lint`



<br>



## basic-gui-packages
install/setup:
- `sudo pacman -S firefox ark flameshot qalculate-gtk discord thunderbird`


AUR packages:
- https://aur.archlinux.org/packages/slack-desktop



### Flameshot multi-monitor
Edit `Exec` in `/usr/share/applications/org.flameshot.Flameshot.desktop` to:
```bash
Exec=env QT_QPA_PLATFORM=xcb flameshot
```



<br>



## server-web-basic
install/setup:
- `sudo pacman -S nginx mariadb php php-fpm php7 php7-fpm`
- `sudo systemctl enable nginx`
- `sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql`
- `sudo systemctl enable mysql`
- `sudo mysql_secure_installation`
- `sudo systemctl enable php-fpm7`



documentation/reference:
- https://wiki.archlinux.org/title/MariaDB



### server-web-dev
install/setup:
- `sudo pacman -S composer`
- Get and run `wget` or `curl` command for NVM [here](https://github.com/nvm-sh/nvm)
- `nvm install --lts`
- `nvm install v16`
- `nvm use --lts`



### postgresql
TODO: Verify and add extra necessary steps

- `> sudo pacman -S postgresql`
- `> sudo -iu postgres`
- - `> [postgres] initdb -D /var/lib/postgres/data`
- - `> [postgres] createuser --interactive`
- - `> [postgres] createdb myDatabaseName`



#### pg_ugprade


#### initdb
- `> initdb -D /var/lib/postgres/data --locale=en_US.UTF-8 --encoding=UTF8 --data-checksums`


# sudo rm -rd /var/lib/postgres/olddata
- `> mv /var/lib/postgres/data /var/lib/postgres/olddata`
- `> mkdir /var/lib/postgres/data /var/lib/postgres/tmp`
- `> chown postgres:postgres /var/lib/postgres/data /var/lib/postgres/tmp`
- `> cd /var/lib/postgres/tmp`
- `> sudo -su postgres`
- `> [postgres] initdb -D /var/lib/postgres/data --locale=en_US.UTF-8 --encoding=UTF8 --data-checksums`

Replace `PG_VERSION`` with old version:
- `> [postgres] pg_upgrade -b /opt/pgsql-PG_VERSION/bin -B /usr/bin -d /var/lib/postgres/olddata -D /var/lib/postgres/data`



<br>



## server-gitlab



### gitlab-runner
See the following:
- https://docs.gitlab.com/runner/install/linux-manually.html
- https://docs.gitlab.com/runner/executors/#i-am-not-sure

Config file: `/etc/gitlab-runner/config.toml`


install/setup:
- `> sudo pacman -S gitlab-runner`


This will create the user "gitlab-runner", etc., but I had some issues with running `gitlab-runner start` or `systemctl start gitlab-runner`, and re-creating the user worked.
- `sudo useradd --comment 'gitlab-runner' --create-home gitlab-runner --shell /bin/bash`


You'll need to configure the runners and start them.


If you're using the SSH runner, you'll need to:
- create an SSH key for the linux user you're using for it
- set the identity-file in the gitlab `config.tml` to point to the user
- potentially set `disable_strict_host_key_checking = true` - see https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29084
- for some reason to use NVM I had to move the things `install.sh` cats into the `~/.bashrc` into `~/.bash_profile` before `~/.bashrc` is laoded


Here's an example `/etc/gitlab-runner/config.tml`

```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "aterlux ssh runner"
  url = "https://gitlab.com/"
  id = redacted
  token = "redacted"
  token_obtained_at = 2023-03-26T01:49:03Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "ssh"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.ssh]
    user = "gitlab-runner"
    host = "public.ip.goes.here"
    port = "22"
    disable_strict_host_key_checking = true
    identity_file = "/home/gitlab-runner/.ssh/id_ed25519"
```



To unregister runners, do:
- `> sudo gitlab-runner unregister --url https://gitlab.com --token token`

To unregister runners that were removed via GitLab web UI:
- `> sudo gitlab-runner verify --delete`



<br>



## memes

AUR packages:
- https://aur.archlinux.org/packages/activate-linux-git



<br>



## uncategorised-packages



### Remmina (Remote Desktop)
install/setup:
- `sudo pacman -S remmina`
- `sudo pacman -S libvncserver` for VNC support
- `sudo pacman -S spice-gtk` for spice-gtk support
- `sudo pacman -S freerdp` for RDP support
- `killall remmina` to refresh or whatever to get the RDP support to pop up



documentation/reference:
- https://wiki.archlinux.org/title/Remmina
- https://gitlab.com/Remmina/Remmina
- https://www.umkc.edu/is/support/services/homecomputing/remotedesktop/linux/



### Vinagre (Remote Desktop)
Not currently in use.

install/setup:
- `sudo pacman -S vinagre`



<br><br>



# Issues


## Discord not opening in browser
Make sure `xdg-utils` is installed. I already had it installed and everything worked, but Discord is shit and sometimes decides to break itself.


Check default web browser and handlers for URLs:
- `> xdg-settings get default-web-browser`
- `> xdg-mime query default x-scheme-handler/http`
- `> xdg-mime query default scheme-handler/http`
- `> xdg-mime query default x-scheme-handler/https`
- `> xdg-mime query default scheme-handler/https`


Set defaults:
- `> xdg-mime default firefox.desktop x-scheme-handler/http`
- `> xdg-mime default firefox.desktop scheme-handler/http`
- `> xdg-mime default firefox.desktop x-scheme-handler/https`
- `> xdg-mime default firefox.desktop scheme-handler/https`



## DBeaver incompatible JVM
- Error: "Incompatible JVM. Version xxx of the JVM is not suitable for this product. Version: 11 or greater is required"

For some reason I ran into this after updating packages. Curse you Java.


This seemed to fix it:
- `> sudo archlinux-java set java-11-openjdk`



documentation/reference:
- https://forum.manjaro.org/t/dbeaver-does-not-start-on-launch-reference-to-arch-linux-issue/51868
