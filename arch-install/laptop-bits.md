## G14 Specific

- https://wiki.archlinux.org/title/ASUS_GA401I
- https://asus-linux.org/


Add to `/etc/pacman.conf`:
```
[g14]
SigLevel = DatabaseNever Optional TrustAll
Server = https://arch.asus-linux.org
```


- `> sudo pacman -S linux-g14 linux-g14-headers`
- `> vim /etc/default/grub`
	- `GRUB_DEFAULT=saved` - boot last used
	- `GRUB_SAVEDEFAULT=true`  - boot last used
	- `GRUB_DISABLE_SUBMENU=y` - show all options in main screen
- `> sudo grub-mkconfig -o /boot/grub/grub.cfg`


To then install the hardware controls:
- `> sudo pacman -S asusctl supergfxctl`



## current attempt at fixing shit
https://wiki.archlinux.org/title/PRIME

- `> sudo pacman -S xf86-video-amdgpu`

kinda gave up with the above

X11 on a clean boot "works":
- audio works
- discord works
- VSCode works (I do get a systemd-coredump of VSCode with a bunch of stuff as `/home/airi/.config/Code - OSS/User/globalStorage/ms-liveshare.vsliveshare/dotnet-3.1.14/shared/Microsoft.NETCore.App/3.1.14/libcoreclr.so`)
- `sensors` takes a moment to spin up first time and after I think a few mins of not monitoring sensors, journalctl showing `dce110_edp_wait_for_hpd_ready: wait timed out`



## Audio dies a few seconds after idle
- https://unix.stackexchange.com/a/171925




## Laptop stuck with dGPU disabled
- https://asus-linux.org/faq/#switching-to-integrated-hybrid-nvidia-doesn-t-seem-to-work


```bash
echo 0 | sudo tee /sys/devices/platform/asus-nb-wmi/dgpu_disable
echo 1 > sudo /sys/bus/pci/rescan
echo 0 | sudo tee /sys/devices/platform/asus-nb-wmi/dgpu_disable
```

> It may take a few tries to enable it, and a reboot is recommended afterwards.


Also tried uninstalling `xorg-server xorg-xinit sddm sddm-kcm`

... and somehow after all this, and after rebooting, `supergfxctl -s` told me I can choose from integrated, hybrid, and dedicated. and `-S` (current status) told me it's currently `off`


`supergfxctl -V` also now shows `AMD`



## Login loop SDDM and xwayland crash
- https://bbs.archlinux.org/viewtopic.php?id=271370
- https://bbs.archlinux.org/viewtopic.php?id=271370
- https://forum.manjaro.org/t/issue-with-getting-passed-login-screen-stuck-in-a-loop/106068/3


Attached logs:
- [journalctl xwayland crash](./)
- [journalctl login loop](./)
- [strace sddm](./)


The following command seemed to run wayland flawlessy. This was tested with supergfxctl and asusctl uninstalled.
- `> /usr/lib/plasma-dbus-run-session-if-needed /usr/bin/startplasma-wayland` 


In the state where the above command worked and KDE/plasma worked properly, I happened to install a theme and the issue was gone entirely.


*And then after playing around in Windows 10, changing power/fan profiles and GPU mode (but reverted it back to the mode it was in - "Optimized"), xwayland started crashing. This was both with the normal sign in and the hacky command above.*



## Dedicated mode unsupported
Apparently `GFX FAIL: Dedicated mode unsupported on AMD dGPU systems`

I've not tried reproducing this with supergfxctl after reinstall because I don't wanna break stuff, but it seems toggling the MUX switch in Windows and rebooting keeps it in that mode on Linux.



# Network
- https://wiki.archlinux.org/title/Network_configuration/Wireless
- https://wiki.archlinux.org/title/Iwd



## Requisite
The archiso image comes with what you need, but once you've installed Arch to your partition, you'll need to install extra things and then set up `iwctl` again.
- `> pacman -S iwd networkmanager dhcpcd`



## iwctl connect
- `> iwctl device list`
- `> iwctl station <YOUR_INTERFACE> scan`
- `> iwctl station <YOUR_INTERFACE> connect <YOUR_SSID>`


If you don't know your SSID or want to check you're in range:
- `> iwctl station <YOUR_INTERFACE> get-networks`
- `> dhcpcd` to get a DHCP lease. I didn't require this in archiso but did on the installed partition.


### Issue 1
I had some major issues with this, and adding the following to `/etc/iwd/main.conf` seemed to fix it:

```toml
[General]
ControlPortOverNL80211=False
```

References:
- https://bbs.archlinux.org/viewtopic.php?id=260319
- https://www.reddit.com/r/archlinux/comments/jdlayz/iwd_broken_after_update/



## iwctl DNS
Add and modify the following in `/var/lib/iwd/<YOUR_SSID>.<SECURITY_TYPE>`

```toml
[IPv4]
Address=192.168.0.10
Netmask=255.255.255.0
Gateway=192.168.0.1
Broadcast=192.168.0.255
DNS=1.1.1.1
```