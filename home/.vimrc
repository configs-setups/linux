scriptencoding utf-8
set encoding=utf-8


set visualbell
set number
syntax on

"Tab stuff
"See: https://stackoverflow.com/a/3682602/13310905
"See: https://www.reddit.com/r/vim/comments/4hoa6e/what_do_you_use_for_your_listchars/
set autoindent
set noexpandtab
set tabstop=4
set shiftwidth=4

"Render whitespace
set list
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·,eol:¬

set termguicolors
set title
set scrolloff=8
