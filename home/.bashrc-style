#!/bin/bash


# ------------------------------------------------------------------------------
# SECTION PS1

# PS1 with misc prefixes and git branch display toggling
# REQUIRES: git_branch function
if [ "$color_prompt" = yes ]; then
	# Original
	# PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
	# Original with git branch
	# PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(git_branch)\[\033[00m\]\$ '

	# I'm not sure what the debian_chroot thing is supposed to be.

	# No misc, no git branch
	if [ "$TERMINAL_NO_MISC" = 1 ] && [ "$TERMINAL_NO_GITBRANCH" = 1 ]; then
		# PS1="${debian_chroot:+($debian_chroot)}\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
		PS1="\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
	# No misc, show git branch. Use OR to allow TERMINAL_NO_GITBRANCH to be null (default = 1)
	elif [ "$TERMINAL_NO_MISC" = 1 ] && [[ -z "${TERMINAL_NO_GITBRANCH+0}" || "$TERMINAL_NO_GITBRANCH" = 0 ]]; then
		# PS1="${debian_chroot:+($debian_chroot)}\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\[\033[38;5;197m\]\$(git_branch)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
		PS1="\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\[\033[38;5;197m\]\$(git_branch)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
	# Show misc, no git branch. Use OR to allow TERMINAL_NO_MISC to be null (default = 1)
	elif [[ -z "$TERMINAL_NO_MISC" || "$TERMINAL_NO_MISC" = 0 ]] && [ "$TERMINAL_NO_GITBRANCH" = 1 ]; then
		# PS1="${debian_chroot:+($debian_chroot)}\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
		PS1="\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
	# Show misc, show git branch
	else
		# PS1="${debian_chroot:+($debian_chroot)}\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\[\033[38;5;197m\]\$(git_branch)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
		PS1="\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\[\033[38;5;197m\]\$(git_branch)\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"
	fi
else
	# Original
	# PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
	# Original with git branch
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w$(git_branch)\$ '
fi
# unset color_prompt force_color_prompt

# --------------------
# Toggle miscellaneous first bits in terminal
function terminal_show_misc {
	if [ "$TERMINAL_NO_MISC" = 1 ]; then
		export TERMINAL_NO_MISC=0
		source ~/.bashrc-style
	else
		export TERMINAL_NO_MISC=1
		source ~/.bashrc-style
	fi
}

# Toggle git branch in terminal
function terminal_show_branch {
	if [ "$TERMINAL_NO_GITBRANCH" = 1 ]; then
		export TERMINAL_NO_GITBRANCH=0
		source ~/.bashrc-style
	else
		export TERMINAL_NO_GITBRANCH=1
		source ~/.bashrc-style
	fi
}
# --------------------



# --------------------
# PS1 formats
# Lengthy display, command on new line
# `shellName terminal timeWithSeconds username@hostname directory \n >`
# export PS1="\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"

# Lengthy display, command on new line
# `terminal timeWithSeconds username@hostname:directory \n >`
# export PS1="\[\033[38;5;39m\]\l\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;231m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;226m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;45m\]>\[$(tput sgr0)\]"

# Compact, command on new line
# `username@hostname:directory \n >`
# export PS1="\[\033[38;5;220m\]\u@\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;39m\]>\[$(tput sgr0)\]"

# Lengthy display with "PROD" prefix, command on new line
# `PROD shellName terminal timeWithSeconds username@hostname:directory \n >`
# export PS1="\[\033[38;5;196m\]PROD \[$(tput sgr0)\]\[\033[38;5;39m\]\v\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;39m\]\l\[$(tput sgr0)\] \t \[$(tput sgr0)\]\[\033[38;5;197m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;220m\]\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;39m\]>\[$(tput sgr0)\]"

# Lengthy display with "PROD" prefix, command on new line
# `PROD terminal timeWithSeconds username@hostname:directory \n >`
# export PS1="\[\033[38;5;196m\]PROD \[$(tput sgr0)\]\[\033[38;5;39m\]\[$(tput sgr0)\]\[\033[38;5;39m\]\l\[$(tput sgr0)\] \t \[$(tput sgr0)\]\[\033[38;5;197m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;220m\]\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;51m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;39m\]>\[$(tput sgr0)\]"
# --------------------

# !SECTION PS1
# ------------------------------------------------------------------------------


