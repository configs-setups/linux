#!/bin/bash
# ~/.bashrc: executed by bash(1) for non-login shells.

# function terminal_pure {
# 	if [ "$TERMINAL_PURE" = 1 ]; then
# 		echo "setting to 0"
# 		export TERMINAL_PURE=0
# 		source ~/.bashrc
# 	else
# 		echo "setting to 1"
# 		export TERMINAL_PURE=1
# 		source ~/.bashrc
# 	fi
# }

# terminal_pure=$(cat /tmp/terminal_pure)
# if [ "$terminal_pure" = 1 ]; then
# 	return 0;
# fi

# ------------------------------------------------------------------------------
# SECTION Basics

# UNSURE if necessary
# If not running interactively, don't do anything
case $- in
	*i*) ;;
	  *) return;;
esac

# UNSURE if necessary
# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# UNSURE if necessary
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# UNSURE if necessary
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi


# SSH hosts auto-complete
# See: https://unix.stackexchange.com/a/181603
_ssh() 
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=$(grep '^Host' ~/.ssh/config ~/.ssh/config.d/* 2>/dev/null | grep -v '[?*]' | cut -d ' ' -f 2-)

    COMPREPLY=( $(compgen -W "$opts" -- ${cur}) )
    return 0
}
complete -F _ssh ssh

# !SECTION Basics
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# SECTION History control

# UNSURE if necessary
# append to the history file, don't overwrite it
shopt -s histappend

# Bash: Don't log commands starting with a space, and don't log duplicates
export HISTCONTROL=ignoreboth

# --------------------
# Unlimited bash history
# Eternal bash history. https://stackoverflow.com/a/19533853/13310905
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
# --------------------

# !SECTION History control
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# SECTION I don't even know

# UNSURE if necessary
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color) color_prompt=yes;;
esac

# !SECTION I don't even know
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# SECTION Extra usability bits

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# !SECTION Extra usability bits
# ------------------------------------------------------------------------------



export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
dir_git="/mnt/d/productivity-nvme/__Git"
dir_aur="/mnt/d/productivity-nvme/__Git/aur-packages"
proj_solid="${dir_git}/aterlux/solidjs-home"
proj_drogon="${dir_git}/aterlux/drogon-home"



# PS1 styling etc.
source ~/.bashrc-style
# General code, aside from Golang
source ~/.bashrc-code
# source ~/.bashrc-go


alias vim=nvim
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias my-ip="ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'"
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias lsls='ls -AFhkl --color'
alias watch-sensors="watch -n 0.1 sensors"
alias watch-cpufreq="watch -n 1 \"cat /proc/cpuinfo | grep \"MHz\"\""
alias xclipc="xclip -selection clipboard"


# Keychain SSH
# To reduce initial launch times, keychain won't be loaded and "key_ssh" alias can be used.
# After first launch, keychain will load and prompt for passwords
# NOTE: The above doesn't work because WSL doesn't clear /tmp ... ffs
# is_bash_first_launch=$(cat /tmp/is_bash_first_launch || "is_first")
# if [ $is_bash_first_launch = "already_launched" ]; then
# 	eval $(keychain --eval --quiet ~/.ssh/keyfile1 ~/.ssh/keyfile2)
# else
# 	echo "already_launched" > /tmp/is_bash_first_launch
# 	alias key_ssh='eval $(keychain --eval --quiet ~/.ssh/keyfile1 ~/.ssh/keyfile2)'
# fi
alias ks='eval $(keychain --eval --quiet ~/.ssh/keyfile1 ~/.ssh/keyfile2)'


source ~/.bashrc-git
source ~/.bashrc-pacman



source ~/.bashrc-wsl
# source ~/.bashrc-laptop




# OPT: Shortcut to grep history for given line numbers, from bottom of file,
# and cat and copy to clipboard the results
# REQUIRES: tac, grep, sed, awk, xclipc alias
# NOTE: Function design to be executed without omitting itself from history, i.e., don't prefix with space
# Yes I'm using grep, awk, and sed, cause I didn't know how to do all of them in one...
function hgrep_offset {
	if [ $# -ne 2 ]; then
		echo "You need to supply 3 args: \"<grep string>\" <start_line> <end_line>"
		echo "Line numbers are offset from last"
		return 0
	fi

	# Also gets +1 to +5 lines (before) and -5 to -1 (after)
	# the desired line
	offset_before_start=$(($2+1))
	offset_before_end=$(($2+5))
	offset_after_start=$(($2-5))
	offset_after_end=$(($2-1))
	# Clamped here, because negative/0/1 values don't work,
	# so we just return everything after value in $2
	if [ 1 -gt "$offset_after_start" ]; then
		offset_after_start=1
	fi

	# Using tac to reverse the list, because we wana go from bottom
	res_actual=$(history | tac | grep "$1" | sed -n "$2,$2p")
	res_before_three=$(history | tac | grep "$1" | sed -n "$offset_before_start,${offset_before_end}p" | tac)
	# If the arg was 1, we can't look for anything *after* the result
	res_after_three=""
	if [ "$offset_after_end" -gt 0 ]; then
		res_after_three=$(history | tac | grep "$1" | sed -n "$offset_after_start,${offset_after_end}p" | tac)
	fi
	echo "---------- Before ---------"
	echo "$res_before_three"
	echo "---------- Actual ---------"
	echo "$res_actual"
	echo "---------- After ---------"
	echo "$res_after_three"
	# awk somehow gets everything after the "]" char,
	# and removes a few of the spaces or something with substr
	# E.g., it works with these:
	# 123  [2023-03-06 21:31:57] cp -r ~/.config/nvim/ .
	# 19175  [2023-03-06 21:31:57] cp -r ~/.config/nvim/ .
	res_actual_clean=$(echo "$res_actual" | awk -F] '{$1="";print substr($0,3)}')
	echo "$res_actual_clean" | xclipc
}

# OPT: Grep for last command in history
# REQUIRES: tac, grep, sed, awk
# NOTE: Function design to be executed without omitting itself from history, i.e., don't prefix with space
function hgrep_single {
	# Same impl as hgrep_offset
	# Using line 2 so we don't accidentally return this command from history
	res=$(history | grep "$1" | tac | sed -n "2,2p" | awk -F] '{$1="";print substr($0,3)}')
	echo "$res"
}

# OPT: Grep for last command in history and copy to clipboard
# REQUIRES: tac, grep, sed, awk, xclipc alias
# NOTE: Function design to be executed without omitting itself from history, i.e., don't prefix with space
function hgrep_csingle {
	# Same impl as hgrep_offset
	# Using line 2 so we don't accidentally return this command from history
	res=$(history | grep "$1" | tac | sed -n "2,2p" | awk -F] '{$1="";print substr($0,3)}')
	echo "$res" | xclipc
	echo "$res"
}

# OPT: ffmpeg libsvtav1 for really smol sizes
function ffmpeg_av1_smol {
	ffmpeg -i "$1" -vf "scale=1280:720" -c:v libsvtav1 -cpu-used 0 -b:v 1M -pass 1 -an -f null /dev/null && \
	ffmpeg -i "$1" -vf "scale=1280:720" -c:v libsvtav1 -cpu-used 0 -b:v 1M -pass 2 output.mkv
}

function ffmpeg_av1_1080p {
	ffmpeg -i "$1" -vf "scale=1920:1080" -c:v libsvtav1 -cpu-used 0 -b:v 10M -pass 1 -an -f null /dev/null && \
	ffmpeg -i "$1" -vf "scale=1920:1080" -c:v libsvtav1 -cpu-used 0 -b:v 10M -pass 2 output.mkv
}

# OPT: ffmpeg vp9 with naive multithreading setting
function ffmpeg_vp9 {
	ffmpeg -i "$1" -c:v libvpx-vp9 -b:v 5M -threads 16 -row-mt 1 ffmpeg_vp9.webm
}

function ffmpeg_vp9_1080p {
	ffmpeg -i "$1" -vf "scale=1920:1080" -c:v libvpx-vp9 -b:v 10M -threads 16 -row-mt 1 ffmpeg_vp9.webm
}

# OPT: ffmpeg veryslow 1080p (meant for VIOFO dashcam footage)
function ffmpeg_h264_veryslow {
	ffmpeg -i "$1" -vcodec libx264 -preset veryslow -vf scale=1920:-1 output.mp4
}

# OPT: ffmpeg veryslow 1080p for all matching files in folder
# Provide arg such as "*.mp4", else function defaults to "*.MP4"
function ffmpeg_h264_veryslow_folder {
	files="*.MP4"
	if [ ! -z "$1" ]; then
		files="$1"
	fi
	for i in $files; do ffmpeg -i "$i" -vcodec libx264 -preset veryslow -vf scale=1920:-1 "${i%.*}_trans.mp4"; done
}

# OPT: template vidlist to concatenate video files
function ffmpeg_merger_acqusition {
	touch vidlist.txt
	echo "file '$PWD/file1.mkv'" > vidlist.txt
	echo "file '$PWD/file2.mkv'" >> vidlist.txt
	vim vidlist.txt
	ffmpeg -f concat -safe 0 -i vidlist.txt -c copy output.mkv
}

function npm_html_minify {
	html-minifier $1 --collapse-whitespace --remove-comments --remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype --minify-css true --minify-js true > $2
}

function hetzner_ssh {
	ssh -p23 username@username.your-storagebox.de
}

source /home/airi/.bash_kraft_completion
