-- Not sure what this is
-- "disable netrw at the very start of your init.lua (strongly advised)"
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Used to reduce the amount of plugins for a "slim install"
vim.g.slim_mode = 0
vim.g.lang_mode = "cpp-drogon"

-- Clear jump list on open, so Ctrl+o and Ctrl+i are clean
vim.api.nvim_create_autocmd("VimEnter", {
	command = ":clearjumps",
})

-- utils = require("ibhagwan_utils")

-- No plugins when root
-- if not utils.is_root() then
	require("plugins")

	-- nvim-tree setup, done here not in ./after/plugins
	-- normal-mode -> H toggles dotfiles
	-- normal-mode -> I toggles gitignore
	-- TODO: Look into https://github.com/nvim-telescope/telescope-file-browser.nvim
	-- as it may be a prettier alternative
	require("nvim-tree").setup({
		filters = {
			-- show dotfiles
			dotfiles = false,
		},
		git = {
			-- don't ignore things not in git repo
			ignore = false,
		},
	})

	-- Comment plugin.
	-- TODO: Figure out how to make new comments be suffixed with a space
	-- TODO: Most likely vim related, but default indenting on multi-line comments is fucked
	-- and adds a tab for no reason.
	-- aka, "// "
	-- :h comment.config
	-- require('Comment').setup({})
-- end

require("my_utils")
require("remap")
require("editor")
