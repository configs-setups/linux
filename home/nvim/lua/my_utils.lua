function TempClearAllmarks()
	vim.cmd("delm! | delm A-Z0-9")
end

function PermaClearAllmarks()
	vim.cmd("delm! | delm A-Z0-9")
	vim.cmd("wshada!")
end
