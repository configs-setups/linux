-- Set leader to space
vim.keymap.set("n", "<Space>", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "

-- past and keep clipboard by default
vim.keymap.set("x", "p", "\"_dP", { desc = "Paste (no copy)" })
vim.keymap.set("x", "<leader>p", "p", { desc = "Paste (with copy)" })

-- d deletes without replacing register, <leader>d as the default cut
vim.keymap.set("n", "d", "\"_d", { desc = "Del (blackhole)" })
vim.keymap.set("v", "d", "\"_d", { desc = "Del (blackhole)" })
vim.keymap.set("n", "D", "\"_D", { desc = "Del to eol (blackhole)" })
vim.keymap.set("v", "D", "\"_D", { desc = "Del line (blackhole)" })
vim.keymap.set("n", "<leader>d", "d", { desc = "Del (with copy)" })
vim.keymap.set("v", "<leader>d", "d", { desc = "Del (with copy)" })
vim.keymap.set("n", "<leader>D", "D", { desc = "Del (with copy)" })
vim.keymap.set("v", "<leader>D", "D", { desc = "Del (with copy)" })

-- c replaces word without replacing register
vim.keymap.set("n", "c", "\"_c", { desc = "Cut/Change (blackhole)" })
vim.keymap.set("v", "c", "\"_c", { desc = "Cut/Change (blackhole)" })
vim.keymap.set("n", "C", "\"_C", { desc = "Cut/Change to eol (blackhole)" })
vim.keymap.set("v", "C", "\"_C", { desc = "Cut/Change line (blackhole)" })

-- yank to system clipboard
vim.keymap.set("n", "<leader>y", "\"+y", { desc = "Yank to sys clipboard" })
vim.keymap.set("v", "<leader>y", "\"+y", { desc = "Yank to sys clipboard" })
vim.keymap.set("n", "<leader>y", "\"+Y", { desc = "Yank to sys clipboard" })

-- yank relative/full file path
vim.keymap.set("n", "<leader>py", ':let @" = expand("%")<cr>', { desc = "Yank relative path to buffer" })
vim.keymap.set("n", "<leader>pY", ':let @" = expand("%:p")<cr>', { desc = "Yank absolute path to buffer" })

-- center on search
vim.keymap.set("n", "n", "nzz", { desc = "Next search (centres)" })
vim.keymap.set("n", "N", "nzz", { desc = "Next search (centres)" })

-- Center when going up/down half a page
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "Up half page (centres)" })
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "Down half page (centres)" })

-- Cursor stays in place when using J to concatenate lines
vim.keymap.set("n", "J", "mzJ`z", { desc = "Concat lines (keeps cursor location)" })

-- Highlight and move selection
vim.keymap.set("v", "<A-j>", ":m '>+1<CR>gv=gv", { desc = "Move selection up" })
vim.keymap.set("v", "<A-k>", ":m '<-2<CR>gv=gv", { desc = "Move selection down" })

-- Rremove recording register shit
vim.keymap.set("n", "q", "<nop>", { desc = "noop" })
vim.keymap.set("n", "q", "<nop>", { desc = "noop" })
vim.keymap.set("v", "q", "<nop>", { desc = "noop" })

-- Format using LSP
-- vim.keymap.set("n", "ff", "<cmd>lua vim.lsp.buf.format()<CR>", { desc = "aaaaaaaaaaaaa" })

-- chmod executable shortcut
vim.keymap.set("n", "<leader>chx", "<cmd>!chmod +x %<CR>", { desc = "chmod +x this file" })

-- terminal remap because there's no way I'm pressing Ctrl+\ Ctrl+n
-- except I probably won't be using terminal mode anyway because fuck that
-- vim.keymap.set("t", "<A-`>", "<C-\\><C-n>")
vim.keymap.set("t", "<C-space>", "<C-\\><C-n>", { desc = "Escape terminal mode" })

-- regex: current word under cursor
-- source: ThePrimeagen; https://github.com/ThePrimeagen/init.lua/blob/master/lua/theprimeagen/remap.lua
vim.keymap.set("n", "<leader>rs", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
	{ desc = "Regex word under cursor (whole buffer)" })
-- Same thing as above, but only for current line
vim.keymap.set("n", "<leader>rl", [[:s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
	{ desc = "Regex word under cursor (single line)" })

-- source: https://stackoverflow.com/a/25072966/13310905
vim.keymap.set("n", "<leader>pl", "/\\%<C-R>=line('.')<CR>l", { desc = "Find in current line (not telescope)" })


vim.keymap.set("n", "<leader>ee", function()
	if (vim.g.lang_mode == "go") then
		return "oif err != nil {<CR>}<ESC>Oreturn err;<ESC>"
	end


	if (vim.g.lang_mode == "cpp-drogon") then
		-- 	try {
		-- 	ret_proto_type::MessageType proto_struct;
		-- 	util::dret::proto::success_with_data(proto_struct, callback);
		-- 	return;
		-- }
		-- DCATCH_DB_EXCEPTION("namespace::class::method", callback)
		return "otry {<CR>" ..
			"<ESC>Oret_proto_type::MessageType proto_struct;<CR>" ..
			"<ESC>Outil::dret::proto::success_with_data(proto_struct, callback);<CR>" ..
			"<ESC>Oreturn;<CR>" ..
			"}<CR>" ..
			"DCATCH_DB_EXCEPTION(\"namespace::class::method\", callback)<ESC>" ..
			-- select the "namespace::class:method"
			"_vi\""
			-- "<C-_><C-v><C-i>\""
	end
end, { expr = true, desc = "Error handling code for curr lang_mode" })

vim.keymap.set("n", "<leader>everythingisfine", ":CellularAutomaton make_it_rain<cr>", { desc = "Nyaa~" })
