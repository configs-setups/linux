-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

local packer = require("packer")

if (vim.g.slim_mode == 0) then
	return packer.startup(function(use)
		-- Package manager
		use { "wbthomason/packer.nvim" }

		use {
			"nvim-tree/nvim-tree.lua",
			-- NOTE: Requires a "patched" nerd font
			-- As I'm using nvim on terminal, that requires the font
			-- of the terminal to be set.
			-- See: https://www.nerdfonts.com/
			requires = {
				"nvim-tree/nvim-web-devicons", -- optional, for file icons
			},
			-- config = [[require("config.nvim-tree")]],
			-- tag = "nightly" -- optional, updated every week. (see issue #1193)
		}
		-- use { 'nvim-tree/nvim-web-devicons' }

		use {
			"nvim-telescope/telescope.nvim", tag = "0.1.4",
			-- or                              , branch = '0.1.1',
			requires = {
				"nvim-lua/plenary.nvim",
			},
		}

		-- Terminal, commands :ToggleX
		-- Currently just doing `:tab new` (or ctrl+t in nvim-tree) + `:tab term` to use terminal,
		-- use {
		-- 	"akinsho/toggleterm.nvim",
		-- 	tag = "*",
		-- 	config = function()
		-- 		require("toggleterm").setup()
		--   end
		-- }

		-- Syntax highlighting uwu
		use {
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate"
		}
		-- Protobuf syntax highlighting nyaa
		use("mitchellh/tree-sitter-proto")
		-- Show sticky function context at top
		-- use { "nvim-treesitter/nvim-treesitter-context" }
		-- use { "nvim-treesitter/playground" }

		-- Pretty statusline
		use {
			"nvim-lualine/lualine.nvim",
			-- requires = { "nvim-tree/nvim-web-devicons", opt = true }
		}

		-- Run tests
		-- use { "vim-test/vim-test" }
		-- File bookmark
		-- use { "theprimeagen/harpoon" }
		-- Branching undo tree
		-- use { "mbbill/undotree" }
		-- Git
		use { "tpope/vim-fugitive" }
		-- Git diff in gutter
		-- use { "airblade/vim-gitgutter" }
		-- Git blame line
		use { "tveskag/nvim-blame-line" }

		-- LSP setup
		use {
			"VonHeikemen/lsp-zero.nvim",
			branch = "v1.x",
			requires = {
				-- LSP Support
				{ "neovim/nvim-lspconfig" }, -- Required
				{ "williamboman/mason.nvim" }, -- Optional
				{ "williamboman/mason-lspconfig.nvim" }, -- Optional

				-- Autocompletion
				{ "hrsh7th/nvim-cmp" }, -- Required
				{ "hrsh7th/cmp-nvim-lsp" }, -- Required
				{ "hrsh7th/cmp-buffer" }, -- Optional
				{ "hrsh7th/cmp-path" }, -- Optional
				{ "saadparwaiz1/cmp_luasnip" }, -- Optional
				{ "hrsh7th/cmp-nvim-lua" }, -- Optional

				-- Snippets
				{ "L3MON4D3/LuaSnip" }, -- Required
				{ "rafamadriz/friendly-snippets" }, -- Optional
			}
		}

		-- Single and multi-line comment plugin
		use {
			"numToStr/Comment.nvim",
			config = function()
				require("Comment").setup()
			end
		}

		-- NOTE comment anchors, etc.
		use {
			"folke/todo-comments.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("todo-comments").setup {
					-- your configuration comes here
					-- or leave it empty to use the default settings
					-- refer to the configuration section below
				}
			end
		}

		-- Markdown preview in browser
		use({
			"iamcco/markdown-preview.nvim",
			run = "cd app && npm install",
			setup = function() vim.g.mkdp_filetypes = { "markdown" } end,
			ft = { "markdown" },
		})

		use {
			"krivahtoo/silicon.nvim",
			branch = "nvim-0.9",
			run = "./install.sh build"
		}

		-- Help UI for keybinds
		use {
			"folke/which-key.nvim",
			config = function()
				vim.o.timeout = true
				vim.o.timeoutlen = 500
				-- require("which-key").setup {
				-- 	-- your configuration comes here
				-- 	-- or leave it empty to use the default settings
				-- 	-- refer to the configuration section below
				-- }
			end
		}

		-- Discord rich presence
		-- See: https://github.com/andweeb/presence.nvim/wiki/Rich-Presence-in-WSL
		-- use { "andweeb/presence.nvim" }

		-- Themes
		-- https://github.com/rose-pine/neovim
		-- https://github.com/ellisonleao/gruvbox.nvim
		-- https://github.com/nyoom-engineering/nyoom.nvim
		-- https://github.com/folke/tokyonight.nvim
		-- use {
		-- 	"rose-pine/neovim",
		-- 	as = "rose-pine",
		-- 	config = function()
		-- 		require("rose-pine").setup()
		-- 		vim.cmd("colorscheme rose-pine")
		-- 	end
		-- }
		use { "ellisonleao/gruvbox.nvim" }

		-- fun
		use { 'eandrju/cellular-automaton.nvim' }
	end)
else
	return packer.startup(function(use)
		use { "wbthomason/packer.nvim" }
		use {
			"nvim-tree/nvim-tree.lua",
			requires = {
				"nvim-tree/nvim-web-devicons", -- optional, for file icons
			},
		}
		use {
			"nvim-telescope/telescope.nvim", tag = "0.1.4",
			requires = {
				"nvim-lua/plenary.nvim",
			},
		}
		use {
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate"
		}
		use("mitchellh/tree-sitter-proto")
		use {
			"nvim-lualine/lualine.nvim",
		}
		use { "tpope/vim-fugitive" }
		use {
			"numToStr/Comment.nvim",
			config = function()
				require("Comment").setup()
			end
		}
		use {
			"folke/todo-comments.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("todo-comments").setup { }
			end
		}
		use { "ellisonleao/gruvbox.nvim" }
	end)
end
