vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.autoindent = true
-- TODO: Figure out which way this is, in vim I had "set noexpandtab"
vim.opt.expandtab = false
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

-- Using smartcase + ignorecase so searching ignores cases,
-- unless you explicitly type an upper case char, then it's
-- smart about it and doesn't ignore that case.
vim.opt.smartcase = true
vim.opt.ignorecase = true

vim.opt.list = true
-- my newline chars, don't know how to get working in nvim
--vim.opt.listchars = tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·,eol:¬

vim.opt.title = true
vim.opt.scrolloff = 8

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

-- Removes highlight when search. It gets annoying once done searching
vim.opt.hlsearch = false

-- Don't show mode as statusline shows it
vim.opt.showmode = false

vim.opt.nuw = 1


-- NOTE: vim.opt.laststatus set in lualine.lua
