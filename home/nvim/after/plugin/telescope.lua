-- Telescope
local telescope = require("telescope")
local builtin = require("telescope.builtin")

telescope.setup {
	defaults = {
		-- always ignore git and so on
		file_ignore_patterns = {
			"^.git/",
			".git/",
			"node_modules",
			-- php
			"vendor/",
			-- temporary ignores for CPP project,
			-- need to make a way to do this per-workspace
			"third_party",
			"third_party_tools",
			"^.cache/",
			"build/",
		},
	},
}

-- Telescope bindings
-- Ignores .gitignore, shows hidden files, dotenv files, etc..
vim.keymap.set("n", "<Leader>pf", function()
	builtin.find_files({ hidden = true, no_ignore = true })
end, { noremap = true, silent = true, desc = "Fuzzy: All files" })


-- Fuzzy find files in git
-- vim.keymap.set("n", "<C-p>", builtin.git_files, {})
vim.keymap.set("n", "<leader>pg", builtin.git_files, { desc = "Fuzzy: Git files" })

-- Show vim buffers
vim.keymap.set("n", "<leader>pb", builtin.buffers, { desc = "Fuzzy: vim buffers" })

-- Show definitions. Helpful for browsing long files
vim.keymap.set("n", "<leader>pd", function()
	builtin.lsp_document_symbols()
end, { buffer = bufnr, desc = "Fuzzy: Document symbols" })

-- grep across project
vim.keymap.set("n", "<leader>ps", function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") });
end, { desc = "Fuzzy: Grep" })

-- Show all errors.
-- TODO: Better keymap?
vim.keymap.set("n", "<leader>pv", function()
	builtin.diagnostics()
end, { desc = "Fuzzy: Diagnostics (errors)" })
