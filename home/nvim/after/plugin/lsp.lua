if (vim.g.slim_mode == 1)
then
	return;
end

local lsp_zero = require("lsp-zero").preset({
	name = "minimal",
	set_lsp_keymaps = true,
	manage_nvim_cmp = true,
	suggest_lsp_servers = false,
})

local lspconfig = require("lspconfig")



lsp_zero.ensure_installed({
	-- NOTE: Commented out LSPs/formatters need to be manually installed.
	-- See: https://github.com/williamboman/mason-lspconfig.nvim#available-lsp-servers
	-- "black"
	-- "bufls",
	-- "clang-format",
	"clangd",
	"cmake",
	"eslint",
	-- "golangci-lint",
	"golangci_lint_ls",
	"gopls",
	"intelephense",
	-- "jq",
	"jqls",
	"lua_ls",
	"marksman",
	-- "mypv",
	-- "php-cs-fixer",
	-- "phpcbf",
	-- "phpcs",
	-- "pylint",
	"pylsp",
	"stylelint_lsp",
	-- "tsserver",
	"jsonls"
})

-- (Optional) Configure lua language server for neovim
lsp_zero.nvim_workspace()

lsp_zero.setup()



-- vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, {})
-- vim.keymap.set("n", "<leader>gl", vim.diagnostic.open_float, {})
vim.keymap.set("n", "gl", vim.diagnostic.open_float, { silent = true, noremap = true, desc = "LSP: Open diagnostics" })
vim.keymap.set("n", "ga", vim.lsp.buf.code_action, { silent = true, noremap = true, desc = "LSP: Code actions" })
vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { silent = true, noremap = true, desc = "LSP: Goto declaration" })
vim.keymap.set("n", "gd", vim.lsp.buf.definition, { silent = true, noremap = true, desc = "LSP: Goto definition" })
vim.keymap.set("n", "gi", vim.lsp.buf.implementation,
	{ silent = true, noremap = true, desc = "LSP: Goto implementation" })
vim.keymap.set("n", "K", vim.lsp.buf.hover, { silent = true, noremap = true, desc = "LSP: Hover info" })
-- vim.keymap.set("n", "<leader>gd", vim.lsp.buf.type_definition, { silent = true, noremap = true })

vim.diagnostic.config({
	-- show diagnostics in-line to the right
	virtual_text = true,
	--	signs = true,
	--	update_in_insert = false,
	--	underline = true,
	--	severity_sort = false,
	--	float = true,
})



-- See: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#stylelint_lsp
lspconfig.stylelint_lsp.setup {
	settings = {
		stylelintplus = {
			-- stylelint formatting was causing some wild issues
			-- where previus code was being pasted on :w in a React .tsx file
			autoFixOnSave = false,
			autoFixOnFormat = false,
			-- other settings...
		}
	},
}

-- See: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#eslint
lspconfig.eslint.setup({
	on_attach = function(client, bufnr)
		vim.api.nvim_create_autocmd("BufWritePre", {
			buffer = bufnr,
			command = "EslintFixAll",
		})
	end,
})

lspconfig.pylsp.setup {
	-- on_attach = custom_attach,
	settings = {
		pylsp = {
			plugins = {
				-- formatter options
				black = { enabled = true },
				autopep8 = { enabled = false },
				yapf = { enabled = false },
				-- linter options
				pylint = {
					enabled = true,
					executable = "pylint",
					-- args = {"-m", "pylint", "-f", "json"}
				},
				pyflakes = { enabled = false },
				pycodestyle = { enabled = false },
				-- type checker
				pylsp_mypy = { enabled = true },
				-- auto-completion options
				jedi_completion = { fuzzy = true },
				-- import sorting
				pyls_isort = { enabled = true },
			},
		},
	},
	-- flags = {
	-- 	debounce_text_changes = 200,
	-- },
	-- capabilities = capabilities,
}

-- Format buffer with keybind. For JavaScript/TypeScript, this will
-- not use ESLint, unless explicitly calling EslintFixAll.
-- vim.keymap.set("n", "<leader><C-S>", ":EslintFixAll<CR>", {noremap = true, silent = true})
lsp_zero.on_attach(function(client, bufnr)
	lsp_zero.default_keymaps({ buffer = bufnr })
	local opts = { buffer = bufnr, desc = "LSP: Format buffer" }

	vim.keymap.set("n", "<leader><C-S>", function()
		vim.lsp.buf.format({ async = false, timeout_ms = 10000 })
	end, opts)
end)



-- The below makes fix on save work, but for some reason it doesn't
-- follow my eslintrc files.
-- For now just use :EslintFixAll command
-- See: https://www.reddit.com/r/neovim/comments/ultmx0/how_to_setup_eslint_to_format_on_save_with_nvims/
-- lspconfig.eslint.setup({
--   capabilities = capabilities,
--   flags = { debounce_text_changes = 500 },
--   on_attach = function(client, bufnr)
--     client.resolved_capabilities.document_formatting = true
--     if client.resolved_capabilities.document_formatting then
--       local au_lsp = vim.api.nvim_create_augroup("eslint_lsp", { clear = true })
--       vim.api.nvim_create_autocmd("BufWritePre", {
--         pattern = "*",
--         callback = function()
--           vim.lsp.buf.formatting_sync()
--         end,
--         group = au_lsp,
--       })
--     end
--   end,
-- })
