local lualine = require("lualine")

-- See: https://github.com/nvim-lualine/lualine.nvim
lualine.setup {
	options = {
		-- NOTE: Requires Nerd Font
		icons_enabled = true,
		theme = "auto",
		component_separators = { left = "", right = "" },
		-- section_separators = { left = "", right = ""},
		section_separators = { left = "", right = "" },
		disabled_filetypes = {
			statusline = {},
			winbar = {},
		},
		ignore_focus = {},
		always_divide_middle = true,
		globalstatus = false,
		refresh = {
			statusline = 1000,
			tabline = 1000,
			winbar = 1000,
		}
	},
	sections = {
		lualine_a = { "mode" },
		-- Use vim option "laststatus = 3" to have enough space for branch
		lualine_b = { "branch", "diff", "diagnostics" },
		-- lualine_b = {"diff", "diagnostics"},
		lualine_c = { { "filename", path = 1 } },
		lualine_x = { "encoding", "fileformat", "filetype" },
		-- lualine_y = {"progress"},
		lualine_y = {},
		lualine_z = { "location" }
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { "filename" },
		lualine_x = { "location" },
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	winbar = {},
	-- With many files open, I wanna know the name of inactive files
	-- as the statusline is global
	inactive_winbar = {
		lualine_a = { { "filename", path = 1 } },
	},
	extensions = {}
}

-- This makes the statusline one single line across the bottom
vim.opt.laststatus = 3
