if (vim.g.slim_mode == 1)
then
	return;
end

local wk = require("which-key")

wk.setup {
	icons = {
		-- nerd font icon: nf-fa-folder_open
		-- FIXME: For some reason the folder icons double-up on certain prefixes/groups
		-- Even just using "^" or "a" makes it duplicate; only "+" seems to works as expected
		-- To test: open WhichKey, press `c` and `[` and `]` will show the duplication.
		group = " ",
		-- nerd font icon: nf-fa-angle_double_right
		separator = "",
		breadcrumb = "",
	}
}

wk.add({
	{ "g", desc = "prefix: Defaults, LSP, Comments" },
	{ "<leader>p", desc = "prefix: Telescope, Files, Search" },
	{ "<leader>r", desc = "prefix: Regex" },
	{ "<leader>h", desc = "prefix: GitGutter" },
	-- Only one thing here for now lol
	{ "<leader>g", desc = "prefix: vim-fugitive" },
	-- Only chmod +x here for now
	{ "<leader>c", desc = "prefix: chmod (TEMP)" },
})
