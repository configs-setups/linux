-- Open nvim-tree with relative line numbers
-- for easy jumping around
function toggle_tree_with_line_num()
	vim.cmd.NvimTreeToggle()
	-- reduce width
	vim.opt_local.nuw = 1
	vim.cmd("setlocal relativenumber")
end

vim.keymap.set("n", "<leader>pe", toggle_tree_with_line_num, { desc = "nvim-tree: toggle" })
