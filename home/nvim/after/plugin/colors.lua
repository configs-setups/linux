function ColorMyPencils(color)
	-- Default to gruvbox
	color = color or "gruvbox"

	-- For gruvbox
	vim.o.background = "dark"

	vim.cmd.colorscheme(color)

	-- Transparent background
	-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
	-- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

ColorMyPencils()
