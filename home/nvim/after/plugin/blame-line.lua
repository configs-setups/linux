if (vim.g.slim_mode == 1)
then
	return;
end

-- Git blame line isn't enabled by default on BufEnter, but can be
vim.keymap.set("n", "<leader>gb", ":ToggleBlameLine<cr>", { desc = "Toggle Git Blame Line" })
