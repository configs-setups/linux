# neovim
This is my current neovim config, more or less.


Note that `./plugin/packer_compiled.lua` is auto-generated


# Setup

Install neovim etc., and run `:PackerSync` in neovim.


Dependencies:
- `ripgrep` for `grep_string`



# Tips
- Exit vim with `Ctrl+Z` and use command `fg` to go back to the background instance
- Open another tab with `:tab new` _then_ open a terminal in that tab with `:tab term`, nets you a clean editor and a "background" terminal



# Random notes

Replace in selection:
- `:s/search_term/replace_term/g`

Replace in whole file:
- `:%s/search_term/replace_term/g`

Replace and "repeat":
- `/search_term`
- `cgn` then `.` to repeat 
- OR `c<motion>` then `.` to repeat, but I've not used it like this yet


The use of `ignorecase` also affects substitution, so you can either:
- toggle it with `: set ic!`
- add `I` flag, e.g., `:%s/keys/SomeKeys/gI`



# Significant Remaps
- `p` paste without replacing buffer
- - `<leader>p` paste with normal buffer replace
- `<leader>d` - to "delete" instead of cut
- `y` yank to buffer
- `<leader>y` yank to clipboard



# Keys (normal/visual mode)
Quick tip:
- `i` in visual selections is "exclusive"
- `` in visual selections is "inclusive"
- You can expand visual this by continuing to do `}`, `ap`, `a{`, `i(`, etc.



## General Keys:
- `f<char>` - go to next <char>
- `F<char>` - go to previous <char>
- `r` - replace char
- `R` - replace until esc (essentially overtype mode)
- `w` - go to start of next word
- `e` - go to end of current/next word
- `b` - go to start of current/previous word
- `Shift+a` insert at end of line (not whitespace)
- `Shift+i` insert at start (not whitespace)
- `yiw` - yank word
- `viw` - highlight word
- `ciw` - replace word
- `caw` - replace word, removes space
- `cw` - replace part of word after curor
- `ce` - replace part of word after cursor, different whitespace
- `cb` - replace part of word before cursor
- `ct` followed by a char - cut from cursor up to _but not including_ that char, and enters insert mode (super useful, e.g., for replacing parts of a `function_name` by doing `ct_`0
- - `cf` followed by a char, same as above, but "inclusive" of the char
- `V` - select current line
- `#` - cycle through references of current word
- `~` - toggle case
- `.` - to repeat, e.g., select a block then shift right (indent) with `>` and repeat with `.` 
- `gv` - last visual block


## Selections
- `v%` - highlight matching pair, e.g. when hovering over curly brace. This won't highlight function declaration, but you can start from the closing brace then press `_` (`shift`+`-`). Useful for highlighting function blocks instead of using `}`, as blank lines separate the blocks.
- `vi}` - (or `vi{` or `viB`) select everything inside of `{}`, excluding braces
- `va}` - (or `va{` or `vaB`) select everything inside of `{}`, including braces
- - Useful for selecting whole functions. In langs like JS with destructured params, position cursor at `=> {`
- `vi)` - (or `vi(` or `vib` ) select everything inside of `()`, excluding parentheses
- `va)` - (or `va(` or `vab` ) select everything inside of `()`, including parentheses
- `vap` - select paragraph
- `o` - in visual mode to go to the other end of selection.
- - _Revolutionary_. You could do `vap` -> `}}` -> `o` -> `{{`, for example
- `Ctrl+space` - exit terminal mode

<!-- - `v]}` or `v]M` - select up to function closure from starting cursor (as opposed to doing `v}}}` repeatedly) -->



## Marks
- `mA` - where A can be any capital letter, to set a mark. Using capital lets you jump between buffers
- `'A` - jump to mark A. (can also use backtick)
- `:marks` - default nvim view of marks
- `:delm abC` - delete marks `a`, `b`, and `C`
- `:delmarks!` - delete all marks
- `:Telescope marks` - browse marks with telescope
- `which-keys` plugin will show marks when you press `'`
- `(tilde)]` - go to last char of previously changed/yanked text. Useful for `y10k` -> `(tilde)]`



## Find and Replace; Search and Destroy
- `<leader>ps` Telescope grep across project

Mappings:
- `<leader>rs` finds current word under cursor and templates a vim replacement command (for whole file)


`:args` seems to be a powerful concept but I've only used it once thus far

See: https://vi.stackexchange.com/a/2777

Example to find a set of files, then replace a string in those files (note: backticks are required to give to vim after `:args`):
- :args `find . -path './node_modules' -prune -o -type f \( -name '*.tsx' -o -name '*.ts' \)`
- `:args` to see list
- `:argsdo` to work on those files, e.g.:
- - :argdo %s/set_hp_per_animal/update_animals_hp/g 



### Quickfix list
- `ctrl+q` in telescope to put the current files into the quickfix list
- `:cdo s/mystr/my_other_str/g` - example to search and replace across files in the quickfix list
- `:cdo s/mystr/my_other_str/g | update` - also saves files automatically



## Inclusive/exclusive delete
- By default, vim `db` and `dw` won't delete the char at the cursor
- You can toggle exclusive mode with `v`, e.g., `dvb` or `dvw`



## Comment out blocks
- `Ctrl+V` -> select lines -> `I` -> insert, e.g., `//`. Use escape key not `Ctrl+C` to commit action.



## Uncomment out blocks
- `Ctrl+V` with cursor on the comment char -> select lines -> `x`, then `.` to repeat



# Keys (insert mode)
- `<esc>` - exit insert mode
- `Ctrl+c` - exit insert mode, with implications like cancelling certain actions (e.g., `Ctrl+v` block insert)
- `Ctrl+t` - insert mode indent
- `Ctrl+d` - insert mode de-indent
- `Ctrl+w` - del word, think ctrl+backspace in regular editors



# Editor splitting, size, movement
- `Ctrl+w Ctrl+w` will cycle through open windows
- `Ctrl+w` -> followed by h/j/k/l will move in that direction
- `Ctrl+w` -> `Shift+*motion*` will move a window in that direction
- `:res +10` (or using negatives) will resize the current window
- `10Ctrl+w >` or `<` to vertically resize curent window by given amount.
- - `>` increases size, `<` decreases
- `10Ctrl+w +` or `-` to horizontally resize current window by given amount.
- - `+` increases size, `-` decreases
- `Ctrl+^` - go to previously open file, ignores jumps in current file e.g. with `{` and `}`
- `:e#` - go to previously open file, or replace `#` with a number to go back that many
- `Ctrl+u` - go up half a page (moves cursor)
- `Ctrl+d` - go up half a page (moves cursor)
- `Ctrl+b` - go up (back) a page (moves cursor)
- `Ctrl+f` - go up (forward) a page (moves cursor)
- `zz` - move view to centre the cursor (keeps cursor pos) (CARE: `ZZ` is save and exit)
- `zt` - move view to make cursor at the top of screen (keeps cursor pos)
- `zb` - move view to make cursor at the bottom of screen (keeps cursor pos)
- `gt` - go forward a tab
- `gT` - go backward a tab
- `2gt` - go to tab 2, or any number



# Plugin keybinds, references, and so on
- https://github.com/VonHeikemen/lsp-zero.nvim/blob/v1.x/doc/md/lsp.md#default-keybindings
- https://github.com/nvim-tree/nvim-tree.lua
- - https://docs.rockylinux.org/books/nvchad/nvchad_ui/nvimtree/
- https://github.com/rose-pine/neovim
- https://github.com/tpope/vim-fugitive
- https://github.com/airblade/vim-gitgutter
- https://github.com/akinsho/toggleterm.nvim?tab=readme-ov-file
- - Currently just using nvim `:tab new` -> `:tab term` 



## nvim-tree
- `g?` for help
- `-` go up directory
- `W` collapse all
- `E` expand all
- `R` re-read files
- `Ctrl+t` open file in new tab
- `Ctrl+v` open file in vertical split (`h` instead for horizontal but doesn't work for me)
- `Ctrl+k` view meta-data
- `Ctrl+]` to enter a folder
- `-` to go up a folder, keeps your folders things expanded
- `Enter` on the `..` in nvim-tree to go up a folder and collapse open folders



## Undotree plugin
- `Ctrl+w` -> `h` to go down list of undos



## vim-gitgutter
- `<leader>hp` to view git diff on current line (normal mode)



## vim-fugitive (Git)
When in vim fugitive (enter via `<leader>gs`);
- `=` to view changes of file under cursor
- `s` to stage changes of file, or a selected range
- `X` to revert change. **NOTE**: doesn't ask for confirmation and reverts regardless if the change is staged or unstaged
- in diff view of a change, select a range and use `:diffput` to stage the new change, and `:diffget` to keep the old change.
- `cc` or `ca` when selecting last commit in vim-fugitive allows updating that commit -- it will update it with current staged changes



#### TODO:
- Add vim fugitive GBrowse thingies: https://github.com/shumphrey/fugitive-gitlab.vim
- Consider [nvim-treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects) for better navigation of
"blocks" like functions, classes, etc., in place of having to do `va}` or `va)`

