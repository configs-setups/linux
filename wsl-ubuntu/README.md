# wsl-ubuntu



## Stupid bell
Set `set bell-style none` in `/etc/inputrc`



## Vim clipboard
See:
- https://waylonwalker.com/vim-wsl-clipboard/
- https://superuser.com/questions/1291425/windows-subsystem-linux-make-vim-use-the-clipboard


My solution involved the following in `~/.vimrc`:

```vim
if system('uname -r') =~ "Microsoft"
	augroup Yank
		autocmd!
		"autocmd TextYankPost * :call system('/mnt/C/windows/system32/clip.exe ',@")
		autocmd TextYankPost * if v:event.operator ==# 'y' | call system('clip.exe', @0) | endif
		augroup END
endif
```


## Notes

Install Qt (I don't know if this is still relevant)
- https://stackoverflow.com/a/48147461
